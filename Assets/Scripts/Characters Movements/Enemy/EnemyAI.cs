﻿using UnityEngine;
using System.Collections;

public class EnemyAI : MonoBehaviour
{

    public Transform player;
    public float playerDistance;
    public float rotationDamping;
    public float moveSpeed;
    public static bool isPlayerAlive = true;
    Animator anim;
    Rigidbody rigidBody;
    bool isStrike = false;

    void Start()
    {
        anim = GetComponent<Animator>();
        rigidBody = GetComponent<Rigidbody>();
    }


    void Update()
    {
        if (isPlayerAlive)
        {
            
            playerDistance = Vector3.Distance(player.position, transform.position);
            if (playerDistance < 15f)
                LookAtPlayer();
            if (playerDistance < 12f)
                if (playerDistance > 2f && !isStrike)
                    Chase();
                else if (playerDistance < 2f && !isStrike)
                    Attack();
        }
    }

    private void Attack()
    {
        isStrike = true;
        //RaycastHit hit;
        //if (Physics.Raycast(transform.position, transform.forward, out hit))
        //    if (hit.collider.gameObject.tag == "Player")
        //        hit.collider.gameObject.GetComponent<PlayerHealth>().health -= 5f;
    }

    private void LookAtPlayer()
    {
        Quaternion rotation = Quaternion.LookRotation(player.position - transform.position);
        transform.rotation = Quaternion.Slerp(transform.rotation, rotation, Time.deltaTime * rotationDamping);
    }

    void Chase()
    {
        isStrike = false;
        transform.Translate(Vector3.forward * moveSpeed * Time.deltaTime);
        anim.Play("HumanoidWalk");
    }

}
