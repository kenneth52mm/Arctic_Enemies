﻿using UnityEngine;
using System.Collections;

public class SightScript : MonoBehaviour
{
    AIEnemy enAi;
    CharacterStats charStat;

    void Start()
    {
        enAi = GetComponentInParent<AIEnemy>();
        charStat = GetComponentInParent<CharacterStats>();
    }

    void OnTriggerEnter(Collider other)
    {

        // if (other.gameObject.GetComponent<CharacterStats>())
       // Debug.Log(other.gameObject.name);
        if (other.tag.Equals("Player"))
            if (!enAi.enemies.Contains(other.gameObject))
                enAi.enemies.Add(other.gameObject);
    }

    void OnTriggerExit(Collider other)
    {
        //if (enAi.enemies.Contains(other.gameObject))
        //{
        //    enAi.enemies.Remove(other.gameObject);
        //}
    }

}
