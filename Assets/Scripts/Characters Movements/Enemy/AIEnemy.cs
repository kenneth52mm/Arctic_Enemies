﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine.UI;

public class AIEnemy : MonoBehaviour
{
    NavMeshAgent agent;
    CharMove charMove;

    public GameObject wayPointHolder;
    public List<Transform> wayPoints = new List<Transform>();

    float targetTolerance = 1;

    int wayPointIndex;

    Vector3 targetPos;
    Animator anim;
    WeaponManager weaponManager;
    WeaponManager.WeaponType weaponType;

    float patrolTimer;
    public float waitingTime = 4;
    public float attackRate = 4;
    public float rotationDamping = 4;
    public float moveSpeed = 2;
    float attackTimer;
    int mask = ~(1 << 8);
    public List<GameObject> enemies = new List<GameObject>();
    public GameObject enemyToAttack;

    public List<Transform> AvailableCovers = new List<Transform>();
    public GameObject closestCover;

    public enum AIState
    {
        Patrol, Attack, Chase
    }
    public AIState aiState;

    [SerializeField]
    public IK ik;
    [System.Serializable]
    public class IK
    {
        public Transform spine;
        public float aimingZ = 213.46f;
        public float aimingY = -65.93f;
        public float aimingX = 20.1f;
        public float point = 30;
        public bool debugAim;
    }

    public Slider enemyHealth;

    Rigidbody rigidbody;
    void Awake()
    {
       
    }

    void Start()
    {
       
        agent = GetComponentInChildren<NavMeshAgent>();
        charMove = GetComponent<CharMove>();
        anim = GetComponent<Animator>();
        weaponManager = GetComponent<WeaponManager>();
        rigidbody = GetComponent<Rigidbody>();
        Transform[] wayp = wayPointHolder.GetComponentsInChildren<Transform>();

        foreach (Transform tr in wayp)
        {
            if (tr != wayPointHolder.transform)
                wayPoints.Add(tr);
        }
    }

    void CorrectIK()
    {
        weaponType = weaponManager.weaponType;

        if (!ik.debugAim)
        {
            switch (weaponType)
            {
                case WeaponManager.WeaponType.Pistol:
                    ik.aimingZ = 10.72f;
                    ik.aimingX = 14.6f;
                    ik.aimingY = 54.1f;
                    break;
                case WeaponManager.WeaponType.Rifle:
                    ik.aimingZ = 0.72f;
                    ik.aimingX = 14.6f;
                    ik.aimingY = 54.1f;
                    break;
            }
        }
    }

    void Update()
    {
        if (enemyToAttack==null)
        {
            enemyToAttack = GameObject.FindGameObjectWithTag("Player");
            enemyHealth = GameObject.FindGameObjectWithTag("HealthBar").GetComponent<Slider>();
        }
        DecideState();
        switch (aiState)
        {
            case AIState.Attack:
                Attck();
                break;
            case AIState.Patrol:
                Patrolling();
                break;
            case AIState.Chase:
                Chase();
                break;
        }
    }

    void DecideState()
    {
        //if (enemies.Count > 0)
        //{
        if (!enemyToAttack)
        {

            foreach (GameObject enGo in enemies)
            {
                Debug.Log("no enemy to attack");
                Vector3 direction = enGo.transform.position - transform.position;
                float angle = Vector3.Angle(direction, transform.forward);
                if (angle < 110f * 0.5f)
                {
                    RaycastHit hit;
                    if (Physics.Raycast(transform.position + transform.up, direction.normalized, out hit, 15, mask))
                    {
                        if (hit.collider.gameObject.GetComponent<CharacterStats>())
                        {
                            Debug.Log("enemy found");
                            if (hit.collider.gameObject.GetComponent<CharacterStats>().Id != GetComponent<CharacterStats>().Id)
                            {

                                enemyToAttack = hit.collider.gameObject;
                            }
                        }
                    }
                }
            }
        }
        else
        {
            // Debug.Log("enemy sight");
            float distance = Vector3.Distance(enemyToAttack.transform.position, transform.position);
            if (distance < 40f)
            {
                if (distance > 15f)
                    aiState = AIState.Chase;
                if (distance < 14f)
                    aiState = AIState.Attack;
            }
            else
            {
                aiState = AIState.Patrol;
            }
            // Debug.Log(distance);
        }
        //}
    }

    void Attck()
    {
        CorrectIK();
       // rigidbody.constraints = RigidbodyConstraints.FreezePositionY;
        agent.Stop();
        anim.SetFloat("Turn", 0);
        anim.SetFloat("Forward", 0);
        weaponManager.aim = true;
        charMove.Move(Vector3.zero, true, Vector3.zero);
        //Vector3 direction = enemyToAttack.transform.position - transform.position;
        //float angle = Vector3.Angle(direction, transform.forward);

        //if (angle < 110f * 0.5f)
        //{
        //    transform.LookAt(enemyToAttack.transform.position);
        //}
        LookAtPlayer();
        attackTimer += Time.deltaTime;
        if (attackTimer > attackRate)
        {
            ShootRay();
            attackTimer = 0;
        }

        //Debug.Log("attacking");
    }

    private void ShootRay()
    {
        //Debug.Log("attack!");
        float health= enemyToAttack.GetComponent<CharacterStats>().Health;
        if (health > 5)
        {
            RaycastHit hit;
            GameObject go = Instantiate(weaponManager.BulletPrefab, transform.position, Quaternion.identity) as GameObject;
            LineRenderer line = go.GetComponent<LineRenderer>();
            Vector3 startPos = weaponManager.ActiveWeapon.bulletSpawn.TransformPoint(Vector3.zero);
            Vector3 endPos = Vector3.zero;
            //int mask = ~(1 << 8);

            //Vector3 directionToAttack = (enemyToAttack.transform.position - transform.position);
            //Ray ray = new Ray(startPos+new Vector3(0,1,0), directionToAttack);
            //if (Physics.Raycast(startPos + new Vector3(0, 1, 0), directionToAttack, out hit, Mathf.Infinity, mask))
            ////if (Physics.Raycast(ray, out hit, Mathf.Infinity, mask))
            //{
            //    float distance = Vector3.Distance(weaponManager.ActiveWeapon.bulletSpawn.transform.position, hit.point);
            //    RaycastHit[] hits = Physics.RaycastAll(startPos, hit.point - startPos, distance);
            //    foreach (RaycastHit hit2 in hits)
            //    {
            //        if (hit2.transform.GetComponent<Rigidbody>())
            //        {
            //            hit2.transform.GetComponent<CharacterStats>().Health--;
            //        }
            //        //else if(hit2.transform.GetComponent<Destructible>())
            //        //{
            //        //    hit2.transform.GetComponent<Destructible>().destruct = true;
            //        //}
            //    }
            //    //Vector3 direction = hit.transform.position - transform.position;
            //    //direction = direction.normalized;
            //    //hit.transform.GetComponent<Rigidbody>().AddForce(direction * 200);
            //    //hit.transform.GetComponent<CharacterStats>().Health--;
            //    //  enemyHealth.value -= weaponManager.ActiveWeapon.DAMAGE;
            //    //enemyHealth.value -= 0.05f;
            //    endPos = hit.point;
            //  //  endPos.y += 0.5f;
            //    enemyToAttack.GetComponent<CharacterStats>().Health--;

            //}
            // endPos = enemyToAttack.transform.position;
            endPos = enemyToAttack.transform.localPosition;
            //if (Application.loadedLevel == 7)
            //    endPos.y = 3;
            //if (Application.loadedLevel == 6)
            //    endPos.y = 32.5f;
            //else 
            endPos.y += 1;
            // endPos = enemyToAttack.transform.position - transform.position;
            enemyToAttack.GetComponent<CharacterStats>().Health -= 5;
            enemyHealth.value -= 0.05f;
            go.GetComponent<AudioSource>().Play();
            line.SetPosition(0, startPos);
            line.SetPosition(1, endPos);
        }
    }

    //void OnAnimatorIK()
    //{
    //    if (enemyToAttack)
    //    {
    //        anim.SetLookAtWeight(1, 0.8f, 1, 1, 1);
    //        anim.SetLookAtPosition(enemyToAttack.transform.position);
    //    }
    //    else
    //    {
    //        anim.SetLookAtWeight(0);
    //    }
    //}

    void Patrolling()
    {
        //  rigidbody.constraints = RigidbodyConstraints.FreezeRotationY;
        agent.Resume();
        agent.speed = 1;

        if (wayPoints.Count > 0)
        {
            if (agent.remainingDistance < agent.stoppingDistance)
            {
                patrolTimer += Time.deltaTime;
                if (patrolTimer >= waitingTime)
                {
                    if (wayPointIndex == wayPoints.Count - 1)
                    {
                        wayPointIndex = 0;
                    }
                    else
                    {
                        wayPointIndex++;
                    }
                    patrolTimer = 0;
                }
            }
            else
            {
                patrolTimer = 0;
            }
            agent.transform.position = transform.position;
            agent.destination = wayPoints[wayPointIndex].position;

            Vector3 velocity = agent.desiredVelocity * 0.5f;

            charMove.Move(velocity, false, transform.position);
            //Debug.Log("Y pos " + agent.transform.position);
        }
        else
        {
            agent.transform.position = transform.position;

            Vector3 lookPos = (wayPoints.Count > 0) ? wayPoints[wayPointIndex].position : Vector3.zero;

            charMove.Move(Vector3.zero, false, lookPos); ;
        }
        //  Debug.Log("patrolling");
    }

    private void LookAtPlayer()
    {
        Quaternion rotation = Quaternion.LookRotation(enemyToAttack.transform.position - transform.position);
        transform.rotation = Quaternion.Slerp(transform.rotation, rotation, Time.deltaTime * rotationDamping);
    }

    void Chase()
    {
       // rigidbody.constraints = RigidbodyConstraints.None;
        agent.Resume();
        agent.speed = 1;
        // Debug.Log("chasing");
        agent.transform.position = transform.position;
        agent.destination = enemyToAttack.transform.position;

        Vector3 velocity = agent.desiredVelocity * 0.5f;

        charMove.Move(velocity, false, transform.position);
    }
}
