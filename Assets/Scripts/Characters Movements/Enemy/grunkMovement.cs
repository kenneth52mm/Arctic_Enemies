﻿using UnityEngine;
using System.Collections;

public class grunkMovement : MonoBehaviour
{

    public float speed = 6.0F;
    public float jumpSpeed = 8.0F;
    public float gravity = 20.0F;
    public float starAnim = 1.6f;
    private Vector3 moveDirection = Vector3.zero;
    Animation m_amin;
    void Update()
    {
        CharacterController controller = GetComponent<CharacterController>();
        m_amin = GetComponent<Animation>();
        if (controller.isGrounded)
        {

            moveDirection = new Vector3(Input.GetAxis("Horizontal"), 0, Input.GetAxis("Vertical"));
            moveDirection = transform.TransformDirection(moveDirection);
            moveDirection *= speed;
           
            if (Input.GetButton("Jump"))
                moveDirection.y = jumpSpeed;

        }
        moveDirection.y -= gravity * Time.deltaTime;
        controller.Move(moveDirection * Time.deltaTime);
        if (!m_amin.IsPlaying("metarigAction"))
        {
            m_amin["metarigAction"].time = 1.9f;
            m_amin["metarigAction"].speed = 0.5f;
            m_amin.Play("metarigAction");
        }
        else
        {
            if (m_amin["metarigAction"].time > 2.3f)
                m_amin.Stop("metarigAction");
        }
    }
}
