﻿using UnityEngine;
using System.Collections;

public class CustomIK : MonoBehaviour
{
    Animator anim;

    //Bones
    Transform upperArm;
    Transform foreArm;
    Transform hand;

    //target
    public Transform target;
    public Transform elbowTarget;

    public bool IsEnabled;
    public float weight = 1;

    Quaternion upperArmStartRotation;
    Quaternion foreArmStartRotation;
    Quaternion handStartRotation;

    Vector3 targetRelativeStartPosition;
    Vector3 elbowTargetRelativeStartPosition;

    GameObject upperArmAxisCorrection;
    GameObject foreArmAxisCorrection;
    GameObject handAxisCorrection;

    Vector3 lastUpperArmPosition;
    //Vector3 lastTargetPosition;
    //Vector3 lastElbowTargetPosition;


    void Start()
    {
        anim = GetComponentInParent<Animator>();
        hand = GetComponentInParent<CharMove>().leftHand;
        foreArm = hand.parent;
        upperArm = foreArm.parent;

        upperArmStartRotation = upperArm.rotation;
        foreArmStartRotation = foreArm.rotation;
        handStartRotation = hand.rotation;

        elbowTargetRelativeStartPosition = elbowTarget.position - upperArm.position;

        upperArmAxisCorrection = new GameObject("upperArmAxisCorrection");
        foreArmAxisCorrection = new GameObject("foreArmAxisCorrecion");
        handAxisCorrection = new GameObject("handAxisCorrection");

        upperArmAxisCorrection.transform.parent = transform;
        foreArmAxisCorrection.transform.parent = upperArmAxisCorrection.transform;
        handAxisCorrection.transform.parent = foreArmAxisCorrection.transform;

        lastUpperArmPosition = upperArm.position + 5 * Vector3.up;
    }

    void Update()
    {
        if (anim.GetCurrentAnimatorStateInfo(2).IsTag("Aim"))
        {
            IsEnabled = true;
        }
        else 
        {
            IsEnabled = false;
        }
    }

    void LateUpdate()
    {
        if (IsEnabled)
        {
            CalculateIK();
        }
    }

    void CalculateIK()
    {
        if (target == null)
        {
            targetRelativeStartPosition = Vector3.zero;
            return;
        }

        if (targetRelativeStartPosition == Vector3.zero && target != null)
        {
            targetRelativeStartPosition = target.position - upperArm.position;
        }

        //lastUpperArmPosition = upperArm.position;
        //lastTargetPosition = target.position;
        //lastElbowTargetPosition = elbowTarget.position;

        float upperArmLength = Vector3.Distance(upperArm.position, foreArm.position);
        float foreArmLength = Vector3.Distance(foreArm.position, hand.position);
        float armLength = upperArmLength + foreArmLength;
        float hypotenuse = upperArmLength;
        float targetDistance = Vector3.Distance(upperArm.position, target.position);

        targetDistance = Mathf.Min(targetDistance, armLength - 0.0001f);

        float adjacent = (hypotenuse * hypotenuse - foreArmLength * foreArmLength + targetDistance * targetDistance) / (2 * targetDistance);
        float ikAngle = Mathf.Acos(adjacent / hypotenuse) * Mathf.Rad2Deg;

        Vector3 targetPosition = target.position;
        Vector3 elbowTargetPosition = elbowTarget.position;

        Transform upperArmParent = upperArm.parent;
        Transform foreArmParent = foreArm.parent;
        Transform handParent = hand.parent;

        Vector3 upperArmScale = upperArm.localScale;
        Vector3 foreArmScale = foreArm.localScale;
        Vector3 handScale = hand.localScale;
        Vector3 upperArmLocalPosition = upperArm.localPosition;
        Vector3 foreArmLocalPosition = foreArm.localPosition;
        Vector3 handLocalPosition = hand.localPosition;

        Quaternion upperArmRotation = upperArm.rotation;
        Quaternion foreArmRotation = foreArm.rotation;
        Quaternion handRotation = hand.rotation;
        Quaternion handLocalRotation = hand.localRotation;

        target.position = targetRelativeStartPosition + upperArm.position;
        elbowTarget.position = elbowTargetRelativeStartPosition + upperArm.position;
        upperArm.rotation = upperArmRotation;
        foreArm.rotation = foreArmRotation;
        hand.rotation = handStartRotation;

        transform.position = upperArm.position;
        transform.LookAt(targetPosition, elbowTargetPosition - targetPosition);

        upperArmAxisCorrection.transform.position = upperArm.position;
        upperArmAxisCorrection.transform.LookAt(foreArm.position, upperArm.up);
        upperArm.parent = upperArmAxisCorrection.transform;

        foreArmAxisCorrection.transform.parent = foreArm;
        // foreArmAxisCorrection.transform.position = foreArm.position;
        foreArmAxisCorrection.transform.LookAt(foreArm.position, foreArm.up);
        foreArm.parent = foreArmAxisCorrection.transform;

        handAxisCorrection.transform.position = hand.position;
        hand.parent = handAxisCorrection.transform;

        target.position = targetPosition;
        elbowTarget.position = elbowTargetPosition;

        upperArmAxisCorrection.transform.LookAt(target, elbowTarget.position - upperArmAxisCorrection.transform.position);
        upperArmAxisCorrection.transform.localRotation = Quaternion.Euler(upperArmAxisCorrection.transform.localRotation.eulerAngles - new Vector3(ikAngle, 0, 0));
        foreArmAxisCorrection.transform.LookAt(target, elbowTarget.position - upperArmAxisCorrection.transform.position);
        handAxisCorrection.transform.rotation = target.rotation;

        upperArm.parent = upperArmParent;
        foreArm.parent = foreArmParent;
        hand.parent = handParent;
        upperArm.localScale = upperArmScale;
        foreArm.localScale = foreArmScale;
        hand.localScale = handScale;
        upperArm.localPosition = upperArmLocalPosition;
        foreArm.localPosition = foreArmLocalPosition;
        hand.localPosition = handLocalPosition;

        weight = Mathf.Clamp01(weight);
        upperArm.rotation = Quaternion.Slerp(upperArmRotation, upperArm.rotation, weight);
        foreArm.rotation = Quaternion.Slerp(foreArmRotation, foreArm.rotation, weight);
        hand.rotation = target.rotation;


    }
}
