﻿using UnityEngine;
using System.Collections;

[RequireComponent(typeof(ItemID))]
[RequireComponent(typeof(Rigidbody))]
public class WeaponController : MonoBehaviour
{
    public float DAMAGE = 0;
    public bool equip;
    public WeaponManager.WeaponType weapontype;
    public int MaxAmmo;
    public int MaxClipAmmo;
    public int curAmmo;
    public bool CanBurst;
    public float Kickback = 0.1f;

    public GameObject HandPosition;
    public GameObject bulletPrefab;
    public Transform bulletSpawn;
    GameObject bulletSpawnGO;
    ParticleSystem bulletPart;
    WeaponManager parentControl;

    bool fireBullet;
    AudioSource audiosource;
    Animator weaponAnim;
    Rigidbody rigidBody;
    BoxCollider boxCol;

    [Header("Positions")]
    public bool hasOwner;
    public Vector3 EquipPosition;
    public Vector3 EquipRotation;
    public Vector3 UnEquipPosition;
    public Vector3 UnEquipRotation;

    Vector3 scale;

    public enum RestPosition
    {
        RightHip, Waist
    }
    public RestPosition restPosition;

    void Start()
    {
        curAmmo = MaxClipAmmo;
        bulletSpawnGO = Instantiate(bulletPrefab, transform.position, Quaternion.identity) as GameObject;
        bulletSpawnGO.AddComponent<ParticleDirection>();
        bulletSpawnGO.GetComponent<ParticleDirection>().weapon = bulletSpawn;
        bulletPart = bulletSpawnGO.GetComponent<ParticleSystem>();
        //rigidBody = GetComponent<Rigidbody>();
        //rigidBody.isKinematic = true;
        boxCol = GetComponent<BoxCollider>();
        audiosource = GetComponent<AudioSource>();
        weaponAnim = GetComponent<Animator>();
        scale = transform.localScale;
    }

    void Update()
    {
        transform.localScale = scale;
        if (equip)
        {
            transform.parent = transform.GetComponentInParent<WeaponManager>().GetComponent<Animator>().GetBoneTransform(HumanBodyBones.RightHand);
            transform.localPosition = EquipPosition;
            transform.localRotation = Quaternion.Euler(EquipRotation);
            if (curAmmo > 0)
            {
                if (fireBullet)
                {
                    curAmmo--;
                    bulletPart.Emit(1);
                    Debug.Log("shoot");
                    audiosource.Play();
                    //weaponAnim.SetTrigger("Fire");
                    fireBullet = false;

                }
            }
            else
            {
                if (curAmmo >= MaxClipAmmo)
                {
                    curAmmo = MaxClipAmmo;
                    MaxAmmo -= MaxClipAmmo;
                }
                else
                {
                    curAmmo = MaxClipAmmo - (MaxClipAmmo - MaxAmmo);
                }
                //  Debug.Log("Reload");
            }
        }
        else
        {
            if (hasOwner)
            {
                switch (restPosition)
                {
                    case RestPosition.RightHip:
                        transform.parent = transform.GetComponentInParent<WeaponManager>().GetComponent<Animator>().GetBoneTransform(HumanBodyBones.RightUpperLeg);
                        break;
                    case RestPosition.Waist:
                        transform.parent = transform.GetComponentInParent<WeaponManager>().GetComponent<Animator>().GetBoneTransform(HumanBodyBones.Spine);
                        break;

                }
                transform.localPosition = UnEquipPosition;
                transform.localRotation = Quaternion.Euler(UnEquipRotation);

            }

        }

    }

    public void Fire()
    {
        fireBullet = true;
    }
}
