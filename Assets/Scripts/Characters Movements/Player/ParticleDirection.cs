﻿using UnityEngine;
using System.Collections;

public class ParticleDirection : MonoBehaviour {

    public Transform weapon;


    void Start()
    {

    }

    void Update()
    {
        transform.position = weapon.TransformPoint(Vector3.zero);
        transform.forward = weapon.TransformDirection(Vector3.forward);
    }

    void OnParticleCollision(GameObject other)
    {
        if (other.GetComponent<Rigidbody>())
        {
            Vector3 direction = other.transform.position - transform.position;
            direction = direction.normalized;
            other.GetComponent<Rigidbody>().AddForce(direction * 50);    
        }
    
    }
}
