using UnityEngine;
using System.Collections;

public class CharacterStats : MonoBehaviour
{
    public string Id;
    public float Health = 100;

    RagdollManager ragdollManager;
    bool isDead;
    bool doing;
    Animator anim;
    DeathManager deathManager;
    IMessageManager messageManager = new IMessageManager();

    void Start()
    {
        if (Id.Equals("Player"))
        {
            if (GetComponent<DeathManager>())
                deathManager = GetComponent<DeathManager>();
            anim = GetComponent<Animator>();

        }
        else
        {
            if (GetComponent<RagdollManager>())
                ragdollManager = GetComponent<RagdollManager>();

            anim = GetComponent<Animator>();
        }
    }

    void Update()
    {
        if (Id.Equals("Player"))
        {
            if (Health <= 0)
            {
                if (!isDead)
                {
                    if (deathManager != null)
                    {
                        deathManager.RagdollCharacter();
                        CloseAllPlayerComponents();

                    }
                }

                StartCoroutine(Respawn());
                isDead = true;
            }
        }
        else
        {
            if (Health <= 0)
            {
                if (!isDead)
                {
                    if (ragdollManager != null)
                    {
                        ragdollManager.RagdollCharacter();
                        CloseAllComponents();
                        Destroy(gameObject, 30);
                        LevelAchieved.enemiesCount--;
                    }
                }

                isDead = true;
            }
        }
    }

    public void CloseAllComponents()
    {
        if (GetComponent<CharMove>())
        {
            CharMove charMove = GetComponent<CharMove>();
            charMove.enabled = false;
        }

        if (GetComponent<AIEnemy>())
        {
            AIEnemy ai = GetComponent<AIEnemy>();
            ai.enabled = false;
        }

        if (GetComponent<WeaponManager>())
        {
            WeaponManager weaponManager = GetComponent<WeaponManager>();
            weaponManager.enabled = false;
        }

        if (GetComponentInChildren<NavMeshAgent>())
        {
            Debug.Log("has agent");
            NavMeshAgent agent = GetComponentInChildren<NavMeshAgent>();
            agent.enabled = false;
        }

        if (GetComponentsInChildren<WeaponController>().Length > 0)
        {
            Debug.Log("has wp");
            foreach (WeaponController wC in GetComponentsInChildren<WeaponController>())
                wC.enabled = false;
        }

        Collider collider = GetComponent<Collider>();
        collider.enabled = false;

        Rigidbody rigidbody = GetComponent<Rigidbody>();
        rigidbody.isKinematic = true;
    }

    IEnumerator Respawn()
    {
        int counter;

        counter = messageManager.counter;
        doing = messageManager.doing;
        if (!doing && counter == 0)
            StartCoroutine(messageManager.ShowMessage("Game Over", 5));
        yield return new WaitForSeconds(5);
        GameObject.Find("_GameManager").GetComponent<SaveSystem>().LoadState();


    }

    public void CloseAllPlayerComponents()
    {
        if (GetComponent<CharMove>())
        {
            CharMove charMove = GetComponent<CharMove>();
            charMove.enabled = false;
        }

        if (GetComponent<UserInput>())
        {
            UserInput ui = GetComponent<UserInput>();
            ui.enabled = false;
        }

        if (GetComponent<WeaponManager>())
        {
            WeaponManager weaponManager = GetComponent<WeaponManager>();
            weaponManager.enabled = false;
        }

        if (GetComponentsInChildren<WeaponController>().Length > 0)
        {
            Debug.Log("has wp");
            foreach (WeaponController wC in GetComponentsInChildren<WeaponController>())
                wC.enabled = false;
        }

        Collider collider = GetComponent<Collider>();
        collider.enabled = false;

        Rigidbody rigidbody = GetComponent<Rigidbody>();
        rigidbody.isKinematic = true;
    }
    public void HitDetection(Transform hitPart)
    {
        if (hitPart == anim.GetBoneTransform(HumanBodyBones.Head) || hitPart == anim.GetBoneTransform(HumanBodyBones.Chest))
        {
            Debug.Log("found head");
            ragdollManager.RagdollCharacter();
            CloseAllComponents();
            Health -= 100;
            isDead = true;
        }
        else
            Health -= 25;

    }

}