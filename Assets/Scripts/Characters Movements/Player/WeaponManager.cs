﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class WeaponManager : MonoBehaviour
{

    public List<GameObject> WeaponList = new List<GameObject>();
    public WeaponController ActiveWeapon;
    public int weaponNumber = 0;
    public int MaxCarryingWeapons = 2;
    public bool aim;
    public GameObject BulletPrefab;

    public enum WeaponType
    {
        Pistol, Rifle
    }

    public WeaponType weaponType;

    CustomIK customIK;
    Animator anim;

    float IKweight;
    public bool IKTarget;

    [System.Serializable]
    public class IKTargetPos
    {
        [Header("Targets")]
        public Transform HandPlacement;
        public Transform ElbowPlacemet;

        [Header("Elbow Positions")]

        public Vector3 elbowPistolPos = new Vector3(335.1201f, 346.3f, 7.150141f);
        public Vector3 elbowRiflePos = new Vector3(-2.30f, 0.9f, 2.78f);
        public bool DebugIK;

    }
    public IKTargetPos ikTargetPos;

    void Start()
    {
        ActiveWeapon = WeaponList[weaponNumber].GetComponent<WeaponController>();
        if (IKTarget)
        {
            customIK = GetComponentInChildren<CustomIK>();
            ikTargetPos.HandPlacement = ActiveWeapon.HandPosition.transform;
            ikTargetPos.ElbowPlacemet = new GameObject().transform;
            ikTargetPos.ElbowPlacemet.parent = transform;
            customIK.target = ikTargetPos.HandPlacement;
            customIK.elbowTarget = ikTargetPos.ElbowPlacemet;
            Debug.Log("has ik");
        }
        ActiveWeapon.equip = true;
        //ikTargetPos.ElbowPlacemet.parent = transform;
        anim = GetComponent<Animator>();

        foreach (GameObject go in WeaponList)
        {
            go.GetComponent<WeaponController>().hasOwner = true;
        }
    }


    void Update()
    {
        IKweight = Mathf.MoveTowards(IKweight, (aim) ? 1.0f : 0.0f, Time.deltaTime * 5);
        ActiveWeapon = WeaponList[weaponNumber].GetComponent<WeaponController>();
        if (IKTarget)
        {
            ikTargetPos.HandPlacement = ActiveWeapon.HandPosition.transform;
            customIK.target = ikTargetPos.HandPlacement;
        }
        ActiveWeapon.equip = true;
        //customIK.target = ikTargetPos.HandPlacement;

        weaponType = ActiveWeapon.weapontype;
        switch (weaponType)
        {
            case WeaponType.Pistol:
                anim.SetInteger("Weapon", 0);
                //WeaponList[weaponNumber].SetActive(true);
                //WeaponList[weaponNumber + 1].GetComponent<WeaponController>().equip = false;
                //WeaponList[weaponNumber + 1].SetActive(false);
                if (IKTarget)
                    ikTargetPos.ElbowPlacemet.localPosition = ikTargetPos.elbowPistolPos;
                break;
            case WeaponType.Rifle:
                anim.SetInteger("Weapon", 1);
                //WeaponList[weaponNumber].SetActive(true);
                //WeaponList[weaponNumber - 1].GetComponent<WeaponController>().equip = false;
                //WeaponList[weaponNumber - 1].SetActive(false);
                if (IKTarget)
                    ikTargetPos.ElbowPlacemet.localPosition = ikTargetPos.elbowRiflePos;
                break;
        }
        //if (!ikTargetPos.DebugIK)
        //{
        //   
        //}
    }

    void OnAnimatorIK()
    {
        anim.SetIKPositionWeight(AvatarIKGoal.LeftHand, IKweight);
        anim.SetIKRotationWeight(AvatarIKGoal.LeftHand, IKweight);

        Vector3 pos = ActiveWeapon.HandPosition.transform.TransformPoint(Vector3.zero);
        anim.SetIKPosition(AvatarIKGoal.LeftHand, ActiveWeapon.HandPosition.transform.position);
        anim.SetIKRotation(AvatarIKGoal.LeftHand, ActiveWeapon.HandPosition.transform.rotation);

    }
    public void FireActiveWeapon()
    {
        if (ActiveWeapon != null)
        {
            ActiveWeapon.Fire();
        }
    }

    public void ChangeWeapon(bool Asecending)
    {
        if (WeaponList.Count > 1)
        {
           // ActiveWeapon.equip = true;
            if (Asecending)
            {
                if (weaponNumber < WeaponList.Count - 1)
                {
                    weaponNumber++;
                }
                else
                {
                    weaponNumber = 0;
                }
            }
            else
            {
                if (weaponNumber > 0)
                {
                    weaponNumber--;
                }
                else
                {
                    weaponNumber = WeaponList.Count - 1;
                }

            }

        }
    }

    public void ShiftWeapon(int weapon)
    {
        if (WeaponList.Count > 1)
        {
            if (!WeaponList[weaponNumber].GetComponent<WeaponController>().equip)
                return;
            ActiveWeapon.equip = true;
            if (weaponNumber == 1)
            {
                weaponNumber = weapon;
                WeaponList[weaponNumber + 1].GetComponent<WeaponController>().equip = false;
            }
            else
            {
                weaponNumber = weapon;
                WeaponList[weaponNumber - 1].GetComponent<WeaponController>().equip = false;
            }
        }
    }

    internal void ReloadActiveWeapon()
    {
        throw new System.NotImplementedException();
    }
}
