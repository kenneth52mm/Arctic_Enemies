﻿using UnityEngine;
using System.Collections;


public class UserInput : MonoBehaviour
{

    private CharMove character;
    private Transform cam;
    private Vector3 camForward;
    private Vector3 move;

    public Vector3 camNormalState = new Vector3(0, -0.6f, -0);
    public Vector3 camAimingState = new Vector3(0.3f, -0.8f, 0.35f);

    public Vector3 camNormalStateFPS = new Vector3(0, 0.16f, 1.8f);
    public Vector3 camAimingStateFPS = new Vector3(0.36f, 0.39f, 2.21f);
    [HideInInspector]
    public bool fire;
    //[HideInInspector]
    //public Vector3 aimLookPos;
    //public Vector3 aimOffset = Vector3.zero;
    //public bool OverrideAIM=false;
    //private float horizontal ;
    //private float vertical ;

    public bool aim;
    private float aimWeight;
    bool CAMERA;
    public bool lookInCameraDirection;
    Vector3 lookPos;
    Animator anim;

    //CustomIK customIK;
    WeaponManager weaponManager;

    public bool debugShoot;
    WeaponManager.WeaponType weaponType;

    CapsuleCollider col;
    float startHeight;

    bool cameraType = true;

    [SerializeField]
    public IK ik;
    [System.Serializable]
    public class IK
    {
        public Transform spine;
        public float aimingZ = 213.46f;
        public float aimingY = -65.93f;
        public float aimingX = 20.1f;
        public float point = 30;
        public bool debugAim;
    }



    FreeCameraLook cameraFunctions;

    void Start()
    {
        if (Camera.main != null)
        {
            cam = Camera.main.transform;
            CAMERA = true;
        }

        character = GetComponent<CharMove>();
        anim = GetComponent<Animator>();
        //customIK = GetComponentInChildren<CustomIK>();
        weaponManager = GetComponent<WeaponManager>();
        if (CAMERA)
            cameraFunctions = Camera.main.transform.root.GetComponent<FreeCameraLook>();
    }

    void CorrectIK()
    {
        weaponType = weaponManager.weaponType;

        if (!ik.debugAim)
        {
            switch (weaponType)
            {
                case WeaponManager.WeaponType.Pistol:
                    ik.aimingZ = 0.72f;
                    ik.aimingX = 14.6f;
                    ik.aimingY = 54.1f;
                    break;
                case WeaponManager.WeaponType.Rifle:
                    ik.aimingZ = 0.72f;
                    ik.aimingX = 14.6f;
                    ik.aimingY = 54.1f;
                    break;
            }
        }
    }

    void Update()
    {
        CorrectIK();

        if (!ik.debugAim)
            aim = Input.GetMouseButton(1);
        weaponManager.aim = aim;
        if (aim)
        {
            if (!weaponManager.ActiveWeapon.CanBurst)
            {
                if (Input.GetMouseButtonDown(0) || debugShoot)
                {
                    anim.SetTrigger("Fire");
                    //weaponManager.FireActiveWeapon();
                    ShootRay();
                    cameraFunctions.WiggleCrosshairAndCamera(weaponManager.ActiveWeapon, true);
                }
            }
            else
            {
                if (Input.GetMouseButtonDown(0) || debugShoot)
                {
                    anim.SetTrigger("Fire");
                    //weaponManager.FireActiveWeapon();
                    ShootRay();
                    cameraFunctions.WiggleCrosshairAndCamera(weaponManager.ActiveWeapon, true);
                }
            }
        }
        //if (Input.GetAxis("Mouse ScrollWheel") != 0)
        //{
        if (Input.GetKey(KeyCode.Alpha1))
        {
            // weaponManager.ChangeWeapon(false);
            weaponManager.ShiftWeapon(0);
        }
        if (Input.GetKey(KeyCode.Alpha2))
        {
            //weaponManager.ChangeWeapon(true);
            weaponManager.ShiftWeapon(1);
        }
        // }
    }

    public GameObject bulletPrefab;

    void ShootRay()
    {
        float x = Screen.width / 2;
        float y = Screen.height / 2;

        Ray ray = Camera.main.ScreenPointToRay(new Vector3(x, y, 0));
        RaycastHit hit;

        GameObject go = Instantiate(bulletPrefab, transform.position, Quaternion.identity) as GameObject;
        LineRenderer line = go.GetComponent<LineRenderer>();

        Vector3 startPos = weaponManager.ActiveWeapon.bulletSpawn.TransformPoint(Vector3.zero);
        Vector3 endPos = Vector3.zero;
        // int mask = ~(1 << 11);
        int mask = ~(1 << 11);
        if (Physics.Raycast(ray, out hit, 50, mask))
        {
            float distance = Vector3.Distance(weaponManager.ActiveWeapon.bulletSpawn.transform.position, hit.point);

            RaycastHit[] hits = Physics.RaycastAll(startPos, hit.point - startPos, distance);

            foreach (RaycastHit hit2 in hits)
            {
                if (hit2.transform.GetComponent<Rigidbody>())
                {
                    Vector3 direction = hit2.transform.position - transform.position;
                    direction = direction.normalized;

                    if (hit2.transform.root.GetComponent<CharacterStats>())
                    {
                        CharacterStats stats = hit2.transform.root.GetComponent<CharacterStats>();
                        stats.HitDetection(hit2.transform);
                        //RagdollManager ragdollManager = hit2.transform.root.GetComponent<RagdollManager>();
                        //ragdollManager.RagdollCharacter();
                    }

                    // hit2.transform.GetComponent<Rigidbody>().AddForce(direction * 200);
                    // hit2.transform.GetComponent<CharacterStats>().Health-=25;
                }
                //else 
                //{ 
                //}
            }
            endPos = hit.point;
        }
        else
        {
            endPos = ray.GetPoint(10);
        }
        go.GetComponent<AudioSource>().Play();
        line.SetPosition(0, startPos);
        line.SetPosition(1, endPos);

    }

    void LateUpdate()
    {
        //if (OverrideAIM)
        //    aim = true;

        //else
        //aim = Input.GetMouseButton(1);
        aimWeight = Mathf.MoveTowards(aimWeight, (aim) ? 1.0f : 0.0f, Time.deltaTime * 5);
        Vector3 normalState = new Vector3(0, 0, -2f);
        Vector3 aimState = new Vector3(0, 0, -0.5f);
        CameraOption();
        ChangeCameraType();

        //Vector3 pos = Vector3.Lerp(camNormalState, camAimingState, aimWeight);
        //cam.transform.localPosition = pos;


        if (aim)
        {
            Vector3 euluerAngleOffSet = Vector3.zero;
            euluerAngleOffSet = new Vector3(ik.aimingX, ik.aimingY, ik.aimingZ);
            //  Ray ray = Camera.main.ScreenPointToRay(new Vector2(Screen.width / 2, Screen.height / 2));
            Ray ray = new Ray(cam.position, cam.forward);
            Vector3 lookPosition = ray.GetPoint(ik.point);
            //aimLookPos = lookPosition;
            //ik.spine.LookAt(lookPosition);
            //ik.spine.Rotate(euluerAngleOffSet, Space.Self);

        }
    }

    void FixedUpdate()
    {
        float horizontal = Input.GetAxis("Horizontal");
        float vertical = Input.GetAxis("Vertical");

        if (!aim)
        {
            if (cam != null)
            {
                camForward = Vector3.Scale(cam.forward, new Vector3(1, 0, 1)).normalized;
                move = vertical * camForward + horizontal * cam.right;
            }
            else
            {
                move = vertical * Vector3.forward + horizontal * Vector3.right;
            }
        }
        else
        {
            move = Vector3.zero;
            Vector3 dir = lookPos - transform.position;
            dir.y = 0;
            transform.rotation = Quaternion.Slerp(transform.rotation, Quaternion.LookRotation(dir), 20 * Time.deltaTime);
            anim.SetFloat("Forward", vertical);
            anim.SetFloat("Turn", horizontal);
        }

        if (move.magnitude > 1)
            move.Normalize();

        bool walkToggle = Input.GetKey(KeyCode.LeftShift) || aim;
        float walkMultiplier;

        if (walkToggle)
        {

            if (aim)
                walkMultiplier = 0.5f;
            else
                walkMultiplier = 1;
        }
        else
            walkMultiplier = 0.5f;

        // lookPos = lookInCameraDirection && cam != null ? transform.position + cam.forward * 80 : transform.position + transform.forward * 100;
        lookPos = lookInCameraDirection && cam != null ? transform.position + cam.forward * 10 : transform.position + transform.forward * 20;
        move *= walkMultiplier;

        character.Move(move, aim, lookPos);
    }

    const float xPositionTPS = 0;
    const float yPositionTPS = 0.16f;
    const float zPositionTPS = 1.8f;
    const float xPositionFPS = 0.36f;
    const float yPositionFPS = 0.39f;
    const float zPositionFPS = 2.21f;

    /*If true:
        camera type = TPS
      else 
        camera type = FPS
    */
    void ChangeCameraType()
    {
        if (CAMERA)
        {
            //Transform pivot = GameObject.FindGameObjectWithTag("Pivot").transform;
            if (cameraType)
            {
                // pivot.localPosition = new Vector3(xPositionTPS, yPositionTPS, zPositionTPS);
                Vector3 pos = Vector3.Lerp(camNormalState, camAimingState, aimWeight);
                cam.transform.localPosition = pos;
            }
            else
            {
                //pivot.localPosition = new Vector3(xPositionFPS, yPositionFPS, zPositionFPS);
                // camNormalState = new Vector3(xPositionFPS,yPositionFPS,zPositionFPS);
                Vector3 pos = Vector3.Lerp(camNormalStateFPS, camAimingStateFPS, aimWeight);
                cam.transform.localPosition = pos;

            }
        }
    }

    void CameraOption()
    {
        //if (Input.GetKeyDown(KeyCode.Mouse3))
        if (Input.GetMouseButtonDown(2))
        {
            cameraType = !cameraType;
            // cameraFunctions.enabled = false;
        }
    }
}