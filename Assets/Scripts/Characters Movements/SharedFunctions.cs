﻿using UnityEngine;
using System.Collections;

public class SharedFunctions : MonoBehaviour
{

    public static void PickupItem(GameObject owner, GameObject item)
    {
        ItemID.ItemType id = item.GetComponent<ItemID>().itemType;
        switch (id)
        {
            case ItemID.ItemType.Health:

                break;
            case ItemID.ItemType.Weapon:
                WeaponManager wM = owner.GetComponent<WeaponManager>();
                if (wM.WeaponList.Count < wM.MaxCarryingWeapons)
                    wM.WeaponList.Add(item);
                else
                {
                    wM.ActiveWeapon.equip = false;
                    wM.ActiveWeapon.hasOwner = false;
                    wM.ActiveWeapon.transform.parent = null;
                    GameObject removeWeapon = null;
                    foreach (GameObject go in wM.WeaponList)
                    {

                    }
                }
                break;

            case ItemID.ItemType.Weareable:

                break;
        }
    }

    public static bool CheckAmmo(WeaponController AW)
    {
        if (AW.curAmmo > 0)
            return true;
        else
            return false;
    }

    public static bool CompareWeapon(WeaponController weaponOne, WeaponController weaponTwo)
    {
        bool response = false;
        if (weaponOne.weapontype == weaponTwo.weapontype)
        {
            int random = Random.Range(0, 11);

            if (random <= 5)
            {
                response = true;
            }
        }
        else
        {
            if (weaponOne.weapontype == WeaponManager.WeaponType.Rifle)
            {
                response = true;
            }

            if (weaponOne.weapontype == WeaponManager.WeaponType.Pistol)
            {
                if (weaponTwo.weapontype == WeaponManager.WeaponType.Pistol)
                {
                    response = true;
                }
            }
        }
        return response;
    }

    public static int ReturnRandomInt()
    {
        return Random.Range(0,11);
    }

}
