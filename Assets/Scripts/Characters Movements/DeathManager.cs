﻿using UnityEngine;
using System.Collections;

public class DeathManager : MonoBehaviour
{

    public Collider[] colliders;
    public Rigidbody[] rigidbodies;

    Animator anim;
    bool goRagdoll;
    bool isEnemy;

    void Start()
    {
        anim = GetComponent<Animator>();

        rigidbodies = GetComponentsInChildren<Rigidbody>();
        colliders = GetComponentsInChildren<Collider>();

        foreach (Rigidbody rigidbody in rigidbodies)
        {
            if (rigidbody.gameObject.layer == 9)
                rigidbody.isKinematic = true;
        }

        foreach (Collider collider in colliders)
        {
            if (collider.gameObject.layer == 9)
                collider.isTrigger = true;
        }
        // DeactivateRagdoll();
    }

    public void RagdollCharacter()
    {
        if (!goRagdoll)
        {
            anim.enabled = false;
            foreach (Rigidbody rigidbody in rigidbodies)
            {
                if (rigidbody.gameObject.layer == 9)
                    rigidbody.isKinematic = false;
            }

            foreach (Collider collider in colliders)
            {
                if (collider.gameObject.layer == 9)
                    collider.isTrigger = false;
            }
            goRagdoll = true;
            // ActivateRagdoll();
        }
    }

    void ActivateRagdoll()
    {
        anim.enabled = false;
        foreach (Rigidbody rigidbody in rigidbodies)
        {
            rigidbody.isKinematic = false;
        }

        foreach (Collider collider in colliders)
        {
            collider.isTrigger = false;
        }
        goRagdoll = true;
    }

    void DeactivateRagdoll()
    {
        foreach (Rigidbody rigidbody in rigidbodies)
        {
            rigidbody.isKinematic = true;
        }

        foreach (Collider collider in colliders)
        {
            collider.isTrigger = true;
        }
    }
}
