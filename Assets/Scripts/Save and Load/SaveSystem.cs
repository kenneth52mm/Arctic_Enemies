﻿using UnityEngine;
using System.Collections;
using System.Runtime.Serialization.Formatters.Binary;
using System.IO;
using System;

public class SaveSystem : MonoBehaviour
{
    public Transform player;

    void Awake()
    {
        DontDestroyOnLoad(this.transform);
       
        GameObject gm = GameObject.FindGameObjectWithTag("GameManager");

        if (gm != this.gameObject)
            Destroy(gm);

    }

    void Update()
    {
        if (Application.loadedLevel >3 && player==null)
            player = GameObject.FindGameObjectWithTag("Player").transform;
    }

    public void SaveState()
    {
        BinaryFormatter binaryFormater = new BinaryFormatter();
        FileStream file = File.Create(Application.persistentDataPath + "PlayerData.dat");

        PlayerData data = new PlayerData();
        data.xPos = player.transform.position.x;
        data.yPos = player.transform.position.y;
        data.zPos = player.transform.position.z;
        data.xRot = player.transform.eulerAngles.x;
        data.yRot = player.transform.eulerAngles.y;
        data.zRot = player.transform.eulerAngles.z;
        data.level = Application.loadedLevel;

        PlayerPrefs.SetInt("levelToLoad", data.level);

        binaryFormater.Serialize(file, data);
        file.Close();
        Debug.Log("Game Saved on " + Application.persistentDataPath);
    }

    public void LoadState()
    {
        if (File.Exists(Application.persistentDataPath + "PlayerData.dat"))
        {
            BinaryFormatter binaryFormanter = new BinaryFormatter();
            FileStream file = File.Open(Application.persistentDataPath + "PlayerData.dat", FileMode.Open);
            PlayerData data = (PlayerData)binaryFormanter.Deserialize(file);
            file.Close();
            Application.LoadLevel(4);
            player.transform.position = new Vector3(data.xPos, data.yPos, data.zPos);
            player.transform.rotation = Quaternion.Euler(data.xRot, data.yRot, data.zRot);
        }
    }

    public static bool isSaved()
    {
        return File.Exists(Application.persistentDataPath + "PlayerData.dat");
    }

}

[Serializable]
class PlayerData
{
    public float xPos;
    public float yPos;
    public float zPos;

    public float xRot;
    public float yRot;
    public float zRot;

    public int weaponType;
    public int level;
}

