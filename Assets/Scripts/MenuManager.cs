﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class MenuManager : MonoBehaviour
{
    public Transform continueButton;
    Button cont = null;
    SaveSystem saveAndLoad;

    void Awake()
    {
        saveAndLoad = GetComponent<SaveSystem>();
        if (SaveSystem.isSaved())
        {
            cont = continueButton.GetComponent<Button>();
            if (!cont.IsInteractable())
                cont.interactable = true;
        }
    }

    public void LoadGame()
    {
        saveAndLoad.LoadState();
    }

    public void StartGame()
    {
        Application.LoadLevel(Application.loadedLevel + 1);
    }

    public void HowToPlay()
    {
        Application.LoadLevel(12);
    }

    public void Quit()
    {
#if UNITY_EDITOR
        UnityEditor.EditorApplication.isPlaying = false;
#else
		Application.Quit();
#endif
    }
}
