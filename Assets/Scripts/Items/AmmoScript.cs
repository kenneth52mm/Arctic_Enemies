﻿using UnityEngine;
using System.Collections;

public class AmmoScript : MonoBehaviour
{
    int availableAmmo = 30;
    float angle = 360.0f;
    float time = 5.0f;
    Vector3 axis = Vector3.up;

    private IMessageManager messageManager = new IMessageManager();

    void OnTriggerEnter(Collider other)
    {
        if (other.gameObject.tag.Equals("Player"))
        {
            WeaponController weaponController = other.gameObject.GetComponent<WeaponManager>().ActiveWeapon;
            weaponController.curAmmo += 30;
            GetComponent<AudioSource>().Play();
            other.gameObject.GetComponent<Animator>().SetTrigger("Reload");
            StartCoroutine(messageManager.ShowMessage("Ammo +30", 2));
            Destroy(gameObject);
        }

    }

    void Update()
    {
       transform.RotateAround(transform.position,axis,angle*Time.deltaTime/time);
       
    }
}
