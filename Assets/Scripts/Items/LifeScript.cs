﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;
using System;
public class LifeScript : MonoBehaviour
{

    const int life = 25;

    private IMessageManager messageManager = new IMessageManager();

    void OnTriggerEnter(Collider other)
    {
        if (other.gameObject.tag.Equals("Player"))
        {
            CharacterStats stats = other.gameObject.GetComponent<CharacterStats>();
            stats.Health += life;
            GameObject gm = GameObject.FindGameObjectWithTag("HealthBar");
            Slider health = gm.GetComponent<Slider>();          
            health.value += 0.25f;
            StartCoroutine(messageManager.ShowMessage("Life +25", 2));
            Destroy(gameObject);
        }

    }
}
