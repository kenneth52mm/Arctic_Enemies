﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class Checkpoint : MonoBehaviour
{
    //public Transform spawnPoint;
    public GameObject gameManager;
    private IMessageManager messageManager = new IMessageManager();

    void OnTriggerEnter(Collider collider)
    {

        if (collider.tag == "Player")
        {
            SaveSystem saveAndLoad = gameManager.GetComponent<SaveSystem>();
            saveAndLoad.SaveState();
            //spawnPoint.position = new Vector3(transform.position.x, transform.position.y, transform.position.z);
            StartCoroutine(messageManager.ShowMessage("Game Saved!",2));
        }

    }
}
