﻿using UnityEngine;
using System.Collections;

public class LevelAchieved : MonoBehaviour
{
    public static int enemiesCount = 0;
    private IMessageManager messageManager = new IMessageManager();

    void Start()
    {
        enemiesCount = EnemiesByLevel();
    }

    void OnTriggerEnter(Collider collider)
    {
        Debug.Log("Collide achieved");
        if (collider.tag == "Player" && noEnemies())
        {
            SaveGame();
            Application.LoadLevel(9);
        }
        else
            if (collider.tag == "Player" && !noEnemies())
                StartCoroutine(messageManager.ShowMessage("You should to kill all the enemies", 2));
    }

    bool noEnemies()
    {
        return enemiesCount == 0;
    }

    void SaveGame()
    {
        SaveSystem saveAndLoad = GetComponent<SaveSystem>();
        saveAndLoad.SaveState();
    }

    int EnemiesByLevel()
    {
        int resp = 0;
        switch (Application.loadedLevel)
        {
            case 5:
                resp = 6;
                break;

            case 6:
                resp = 10;
                break;
            case 7:
                resp = 15;
                break;
        }
        return resp;
    }
}
