﻿using UnityEngine;
using System.Collections;

public class Respawn : MonoBehaviour {

    public Transform spawnPoint;
    public GameObject player;
    public GameObject gameManager;

    void OnTriggerEnter(Collider collider)
    {
        if (collider.tag == "Player")
        {
            SaveSystem saveAndLoad = gameManager.GetComponent<SaveSystem>();
            saveAndLoad.LoadState();
           // player.transform.position = spawnPoint.position;
        }
    }
}
