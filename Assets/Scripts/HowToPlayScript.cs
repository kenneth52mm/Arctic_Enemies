﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using UnityEngine.UI;

public class HowToPlayScript : MonoBehaviour
{

    public Transform markerLeft2;
    public Transform markerLeft;
    public Transform markerMiddle;
    public Transform markerRight;
    public Transform markerRight2;
    public Transform[] charsPrefabs;
    public static Transform[] charsPrefabsAux;
    public GameObject images;
    public List<Image> hints=new List<Image>();
    private GameObject[] chars;
 
    public static int currentChar = 0;
    public GUISkin SelectGUI;
    private string[] characterNames = new string[] { "1", "2", "3" };

    void Start()
    {
        charsPrefabsAux = charsPrefabs;
        // We initialize the chars array
        chars = new GameObject[charsPrefabs.Length];
       
        // We create game objects based on characters prefabs
        int index = 0;
        foreach (Transform t in charsPrefabs)
        {

            chars[index++] = GameObject.Instantiate(t.gameObject, markerRight2.position, Quaternion.identity) as GameObject;
            if (index == 1)
                chars[index - 1].transform.rotation = Quaternion.EulerAngles(0, 89.5f, 0);
        }

        Image[] i =images.GetComponentsInChildren<Image>();
        foreach (Image img in i)
            hints.Add(img);
       
    }

    void OnGUI()
    {
        GUI.skin = SelectGUI;
        // Here we create a button to choose a next char
        if (GUI.Button(new Rect(10, (Screen.height - 50) / 2, 100, 50), "Previous"))
        {
            currentChar--;

            if (currentChar < 0)
            {
                currentChar = 0;
            }
        }

        // Now we create a button to choose a previous char
        if (GUI.Button(new Rect(Screen.width - 100 - 10, (Screen.height - 50) / 2, 100, 50), "Next"))
        {
            currentChar++;

            if (currentChar >= chars.Length)
            {
                currentChar = chars.Length - 1;
            }
        }

        // Shows a label with the name of the selected character
        GUI.skin.label.alignment = TextAnchor.MiddleCenter;
        // GameObject selectedChar = chars[currentChar];
        string labelChar = characterNames[currentChar];

        GUI.Label(new Rect((Screen.width - 200) / 2, 20, 200, 50), labelChar);

        if (GUI.Button(new Rect(10, Screen.height - 70, 100, 50), "Back"))
        {
            Application.LoadLevel("Main_Screen");
        }

        int middleIndex = currentChar;
        int leftIndex = currentChar - 1;
        int rightIndex = currentChar + 1;
       
        for (int index = 0; index < chars.Length; index++)
        {
            Image img = hints[index];
            if (index < leftIndex)
            {
               // img.transform.position = Vector3.Lerp(img.transform.position, markerLeft2.position, Time.deltaTime);
                img.enabled = false;
                // If the index is less than right index, the character will dissapear in the right side
            }
            else if (index > rightIndex)
            {
              //  img.transform.position = Vector3.Lerp(img.transform.position, markerRight2.position, Time.deltaTime);
                img.enabled = false;
                // If the index is equals to left index, the character will move to the left visible marker
            }
            else if (index == leftIndex)
            {
               // img.transform.position = Vector3.Lerp(img.transform.position, markerLeft.position, Time.deltaTime);
                img.enabled = false;
                // If the index is equals to middle index, the character will move to the middle visible marker
            }
            else if (index == middleIndex)
            {
                //img.transform.position = Vector3.Lerp(img.transform.position, markerMiddle.position, Time.deltaTime);
                img.enabled = true;
                // If the index is equals to right index, the character will move to the right visible marker
            }
            else if (index == rightIndex)
            {
               // img.transform.position = Vector3.Lerp(img.transform.position, markerRight.position, Time.deltaTime);
                img.enabled = false;
            }
        }


    }
}
