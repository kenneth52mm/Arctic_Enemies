﻿using UnityEngine;
using System.Collections;

public class SpawnPositions : MonoBehaviour
{

    public Transform[] spawnPos;

    void Start()
    {
        spawnPos = GetComponentsInChildren<Transform>();
    }

}
