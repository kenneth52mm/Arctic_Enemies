﻿using UnityEngine;
using System.Collections;
using UnityEngine.Networking;

public class NetworkPlayer : NetworkBehaviour
{

    [SyncVar]
    private Vector3 SyncPos;
    [SyncVar]
    private Quaternion SyncRotation;
    [SyncVar]
    private float SyncForward;
    [SyncVar]
    private float SyncTurn;
    [SyncVar]
    private bool SyncAim;
    [SyncVar]
    private int weaponType;
    [SyncVar]
    private Vector3 SyncLookPos;
    [SyncVar]
    private Vector3 SyncShootPos;
    [SyncVar]
    private bool SyncShoot;

    public Transform myTransform;
    public float lerpRate = 15;
    public Transform fps;
    public Camera camera;
    public bool shoot;
    public Vector3 shootPos;

    Vector3 lastPos;
    Quaternion lastRotation;
    Animator anim;
    UserInput uInput;
    public GameObject weapon;
    WeaponManager weaponManager;

    void Start()
    {
        anim = GetComponent<Animator>();
        weaponManager = GetComponent<WeaponManager>();
        weaponManager.enabled = true;
        //camera = Camera.main;
        GameObject wp = Instantiate(weapon, transform.position, Quaternion.identity) as GameObject;
        wp.transform.parent = myTransform;
        weaponManager.WeaponList.Add(wp);

        GetComponent<CharacterStats>().enabled = true;

        if (isLocalPlayer)
        {
            
            SpawnPositions sp = GameObject.FindGameObjectWithTag("SpawnPoint").GetComponent<SpawnPositions>();
            int ran = Random.Range(0, sp.spawnPos.Length - 1);
            Debug.Log(ran); 
            myTransform.position = sp.spawnPos[ran].position;
            GetComponent<CharMove>().enabled = true;
            uInput = GetComponent<UserInput>();
            uInput.enabled = true;

            camera.enabled = true;
            fps.gameObject.SetActive(true);
            fps.parent = null;
        }
        else
        {
            foreach (var childAnimator in GetComponentsInChildren<Animator>())
            {
                if (childAnimator != anim)
                {
                    anim.avatar = childAnimator.avatar;
                    Destroy(childAnimator);
                    break;
                }
            }

            GetComponent<Rigidbody>().isKinematic = true;
        }
    }

    void Update()
    {
        UpdateTransform();
    }

    void FixedUpdate()
    {
        TransmitTransform();
    }

    void UpdateTransform()
    {
        if (!isLocalPlayer)
        {
            myTransform.position = Vector3.Lerp(myTransform.position, SyncPos, Time.deltaTime * lerpRate);
            myTransform.rotation = Quaternion.Slerp(myTransform.rotation, SyncRotation, Time.deltaTime * lerpRate);
            anim.SetBool("Aim", SyncAim);
            anim.SetFloat("Forward", SyncForward);
            anim.SetFloat("Turn", SyncTurn);
            anim.SetInteger("Weapon", weaponType);

            if (SyncShoot)
            {
                DrawShootRay();
                SyncShoot = false;
            }
        }
        else
        { 
        
        }
    }

    void OnAnimatorIK()
    {
        if (!isLocalPlayer)
        {
            if (SyncAim)
            {
                anim.SetLookAtWeight(1, 1, 1, 1);
                anim.SetLookAtPosition(SyncLookPos);
            }
            else 
            {
                anim.SetLookAtWeight(0, 1, 1, 1);
            }
        }
    }

    private void DrawShootRay()
    {
        throw new System.NotImplementedException();
    }

    [Command]
    void Cmd_PassAnimatorValues(float forward,float turn , bool aim, int weapon,Vector3 lPos)
    {
        SyncForward = forward;
        SyncTurn = turn;
        SyncAim = aim;
        weaponType = weapon;
        SyncLookPos = lPos;
    }

    [Command]
    void Cmd_PassShootValues(bool st,Vector3 sPos)
    {
        SyncShoot=st;
        SyncShootPos = sPos;
    }

    [Command]
    void Cmd_PassPosition(Vector3 position)
    {
        SyncPos = position;
    }

    [Command]
    void Cmd_PassRotation(Quaternion rotation)
    {
        SyncRotation = rotation;
    }

    [ClientCallback]
    void TransmitTransform()
    {
        if (isLocalPlayer)
        {
            float distance = Vector3.Distance(myTransform.position, lastPos);
            if (distance > 0.5f)
            {
                Cmd_PassPosition(myTransform.position);
                lastPos = myTransform.position;
            }

            float angle = Quaternion.Angle(myTransform.rotation, lastRotation);
            if (angle > 0.5f)
            {
                Cmd_PassRotation(myTransform.rotation);
                lastRotation = myTransform.rotation;
            }
        }
    }
}
