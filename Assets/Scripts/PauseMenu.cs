using UnityEngine;
using System.Collections;

public class PauseMenu : MonoBehaviour
{

    public GameObject pauseMenuPanel;
    private Animator anim;
    private bool isPaused = false;
    bool wantExit = false;
    public GUISkin SelectGUI;

    void Start()
    {
        Time.timeScale = 1;
        anim = pauseMenuPanel.GetComponent<Animator>();
        anim.enabled = false;

    }

    public void Update()
    {
        if (Input.GetKeyUp(KeyCode.Escape) && !isPaused)
        {
            PauseGame();
        }
        else if (Input.GetKeyUp(KeyCode.Escape) && isPaused)
        {
            UnpauseGame();
        }
    }

    public void PauseGame()
    {
        Time.timeScale = 0;
        isPaused = true;
        //  GameObject.Find("Camera").GetComponent<FreeCameraLook>().a;
        anim.enabled = true;
        anim.Play("PauseMenuSlideIn");


    }
    //function to unpause the game
    public void UnpauseGame()
    {
        Time.timeScale = 1;
        isPaused = false;

        // GameObject.Find("Camera").SetActive(true);
        anim.Play("PauseMenuSlideOut");

    }

    public void BackToMain()
    {
        wantExit = !wantExit;
    }

    void OnGUI()
    {
        GUI.skin = SelectGUI;
        if (isPaused)
        {
            //if (GUI.Button(new Rect((Screen.width / 2) - 120, (Screen.height / 2) - 50, 200, 50), "Resume"))
            //{
            //    isPaused = false;
            //    Time.timeScale = 1;
            //    GUI.enabled = false;
            //}
            //if (GUI.Button(new Rect((Screen.width / 2) - 120, (Screen.height / 2) + 5, 200, 50), "Quit"))
            //{
            //    wantExit = true;
            //}

            if (wantExit)
            {
                GUI.enabled = true;
                if (GUI.Button(new Rect((Screen.width / 2) - 120, (Screen.height + 150) / 2, 100, 50), "Exit"))
                {
                    Application.LoadLevel(13);
                    GUI.enabled = false;
                    wantExit = false;
                    isPaused = false;
                }
                if (GUI.Button(new Rect((Screen.width / 2) - 15, (Screen.height + 150) / 2, 100, 50), "Cancel"))
                {
                    GUI.enabled = false;
                    wantExit = false;
                }
               
            }
        }

       

    }
}