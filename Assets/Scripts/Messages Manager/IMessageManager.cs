﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class IMessageManager
{
    public bool doing;
    public int counter = 0;
    public IEnumerator ShowMessage(string text, float delay)
    {
        GameObject messagesManager = GameObject.FindGameObjectWithTag("Messages");
        Image image = messagesManager.GetComponent<Image>();
        image.enabled = true;
        doing = true;
        Text message = messagesManager.GetComponentInChildren<Text>();
        message.text = text;
        yield return new WaitForSeconds(delay);
        image.enabled = false;
        message.text = " ";
        doing = false;
        counter++;
    }
}
