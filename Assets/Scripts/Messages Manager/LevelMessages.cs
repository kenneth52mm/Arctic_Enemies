﻿using UnityEngine;
using System.Collections;

public class LevelMessages : MonoBehaviour
{

    int level;
    private IMessageManager messageManager = new IMessageManager();
    bool doing = true;
    string[] messages = new string[] { "Take life items to increase your health", "Take ammo items" };
    /*
     
     */
    int counter = 0;
    float time = 0;

    void Awake()
    {
        level = Application.loadedLevel;
    }

    void Start()
    {


    }

    void Update()
    {
        doing = messageManager.doing;
        counter = messageManager.counter;

        switch (level)
        {
            case 5:
                if (!doing && counter == 0)
                    StartCoroutine(messageManager.ShowMessage("Kill your enemies", 5));
                if (!doing && counter == 1 && time > 10f)
                    StartCoroutine(messageManager.ShowMessage("Take life items to increase your health", 4));

                if (!doing && counter == 2 && time > 20f)
                    StartCoroutine(messageManager.ShowMessage("Take ammo items", 2));

                if (!doing && counter == 3 && time > 26f)
                    StartCoroutine(messageManager.ShowMessage("Cross the purple lights to save the game", 5));

                if (LevelAchieved.enemiesCount == 5 && !doing && counter == 4)
                    StartCoroutine(messageManager.ShowMessage("Loarks will attack you each 2 seconds", 5));

                break;

            case 6:
                if (!doing && counter == 0)
                    StartCoroutine(messageManager.ShowMessage("Explore the map to find enemies", 5));

                if (LevelAchieved.enemiesCount == 2 && !doing && counter == 1)
                    StartCoroutine(messageManager.ShowMessage("Climb the peak", 4));
                break;

            case 7:
                if (!doing && counter == 0)
                    StartCoroutine(messageManager.ShowMessage("Explore the map to find enemies", 5));
                if (!doing && counter == 1)
                    StartCoroutine(messageManager.ShowMessage("Be careful, the loarks will be more aggressive", 5));
                break;
        }
        time += Time.deltaTime;
    }

}
