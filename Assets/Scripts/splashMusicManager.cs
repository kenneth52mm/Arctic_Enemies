﻿using UnityEngine;
using System.Collections;

public class splashMusicManager : MonoBehaviour
{

    private AudioSource fxSound; // Emitir sons
    public AudioClip backMusic;

    // Use this for initialization
    void Awake()
    {
        DontDestroyOnLoad(gameObject);
        fxSound = gameObject.AddComponent<AudioSource>();

    }
    void Start()
    {
        fxSound.clip = backMusic;
        fxSound.Play();
    }

    // Update is called once per frame
    void Update()
    {

    }
}
