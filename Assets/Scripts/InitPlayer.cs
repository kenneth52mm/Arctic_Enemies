﻿using UnityEngine;
using System.Collections;

public class InitPlayer : MonoBehaviour
{
    int playerToLoad;

    public GameObject player1;
    public GameObject player2;
    public GameObject player3;

    public GameObject player;

    void Start()
    {
        playerToLoad = PlayerPrefs.GetInt("playerToLoad");
        Vector3 pos = PositionByLevel();
        switch (playerToLoad)
        {
            case 0:
                player = Instantiate(player1,pos, Quaternion.Euler(0, 0, 0)) as GameObject;
                break;

            case 1:
                player = Instantiate(player2, pos, Quaternion.Euler(0, 0, 0)) as GameObject;
                break;

            case 2:
                player = Instantiate(player3, pos, Quaternion.Euler(0, 0, 0)) as GameObject;
                break;
        }
    }


    Vector3 PositionByLevel()
    {
        int levelToLoad = Application.loadedLevel;
        Vector3 resp=Vector3.zero;
        switch (levelToLoad)
        {

            case 5:
                resp = new Vector3(-1.041f, 0, -373f);
                break;
            case 6:
                resp = new Vector3(132.95f, 29.29f, 709.95f);
                break;
            case 7:
                resp = new Vector3(111.24f, 2.011f, 431.612f);
                break;
        }
        return resp;
    }
}
